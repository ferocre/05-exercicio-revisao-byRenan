package br.com.itau.investimento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Aplicacao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAplicacao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	private int idProduto;
	private double valor;
	private String email;
	private int meses;
	
	public int getId() {
		return idAplicacao;
	}
	public void setId(int id) {
		this.idAplicacao = id;
	}
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getMeses() {
		return meses;
	}
	public void setMeses(int meses) {
		this.meses = meses;
	}
}
