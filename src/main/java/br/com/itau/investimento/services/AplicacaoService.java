package br.com.itau.investimento.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class AplicacaoService {
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	public Iterable<Aplicacao> listarAplicacoes(Cliente cliente, int id){
		cliente.setId(id);
		return aplicacaoRepository.findAllByCliente(cliente);
	}
	
}
