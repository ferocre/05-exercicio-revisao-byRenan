	package br.com.itau.investimento.services;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.helpers.GeradorMeses;
import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class SimulacaoService {
	@Autowired
	CatalogoService catalogoService;
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	public List<Simulacao> calcular(Aplicacao aplicacao){
		Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(aplicacao.getIdProduto());
		
		if(!produtoOptional.isPresent()) {
			return null;
		}
		
		aplicacaoRepository.save(aplicacao);
		
		return calcularPorProduto(produtoOptional.get(), aplicacao.getValor(), aplicacao.getMeses());
	}
	
	private List<Simulacao> calcularPorProduto(Produto produto, double valor, int meses){
		List<Simulacao> simulacoes = new ArrayList<Simulacao>();
		Locale locale = new Locale("pt", "BR");
		GeradorMeses gerador = new GeradorMeses();
		
		
		
		for(int i = 0; i < meses; i++) {
			Simulacao simulacao = new Simulacao();
			
			Month mes = gerador.obterProximoMes();
			
			simulacao.setMes(mes.getDisplayName(TextStyle.FULL, locale));
			simulacao.setValor(valor);
			
			valor += valor * produto.getRendimento();
			
			simulacoes.add(simulacao);
		}
		
		return simulacoes;
	}
}
